/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kgteknoloji.goapp.web.rest.vm;
