package com.kgteknoloji.goapp.domain.enumeration;

/**
 * The TodoStatus enumeration.
 */
public enum TodoStatus {
    COMPLETED, PENDING, CANCELLED
}
